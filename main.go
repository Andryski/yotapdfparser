package main

import (
	"./YotaPdfToData"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

func usage() {
	fmt.Fprintf(os.Stderr, "usage: YotaPdfParser <path> <password>\n")
	os.Exit(0)
}

func main() {
	flag.Usage = usage
	flag.Parse()
	temp := flag.NArg()
	if temp != 2 {
		usage()
	}

	paswdArg := flag.Arg(1)
	path := flag.Arg(0)

	cvsPath := path + ".csv"
	if IsFileExists(cvsPath) {
		fmt.Println("csv file alredy exists -" + cvsPath)
		//
		os.Exit(0)
	}

	cvsFile, err := os.Create(cvsPath)
	defer cvsFile.Close()
	if err != nil {
		log.Fatal(err)
	}

	tabel, err := YotaPdfToData.GetContent(path, paswdArg)
	for _, line := range tabel {
		//temp
		//if(line.Number==YotaPdfToData.Internet ||
		//	strings.HasPrefix(line.Type, YotaPdfToData.OutgoingCall) ||
		//		strings.HasPrefix(line.Type, YotaPdfToData.IncomingCall)) {
		//	continue
		//}
		//
		str := line.Data + "\t" + line.Time + "\t" +
			line.Number + "\t" + line.Type + "\t" +
			line.Position + "\t" + line.Target + "\t" +
			line.Volume + "\t" + line.Price
		io.WriteString(cvsFile, str)
		io.WriteString(cvsFile, "\n")
		fmt.Println(str)
	}
}

func IsFileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
