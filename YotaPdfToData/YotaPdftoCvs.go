package YotaPdfToData

import (
	"fmt"
	"github.com/ledongthuc/pdf"
	"os"
	"strings"
)

const Internet = "Интернет"              //Number
const InternetYota = "Интернет Yota"     //Type
const ReceptionSMS = "Прием SMS"         //Type
const OutgoingSMS = "Исходящая SMS: "    //Type
const IncomingCall = "Входящая связь: "  //Type
const OutgoingCall = "Исходящая связь: " //Type

type Row struct {
	Data     string
	Time     string
	Number   string
	Type     string // Вид услуги
	Position string // Место вызова
	Target   string //  Направление вызова
	Volume   string // Объем
	Price    string
}

func GetContent(path string, password string) ([]Row, error) {
	//-19 line
	content, err := readPdf2(path, password) // Read local pdf file
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	//fmt.Println(content)

	//for number, line := range content[:40551]{//[:570*2*8] {
	//	fmt.Println(strconv.Itoa(number)+" "+line)
	//}

	var newContent []string
	for i := 0; i < len(content); i++ {
		if strings.HasPrefix(content[i], "Оператор YOTA (ООО «Скартел»)") {
			//i+=18-1
			i += 10 - 1
			newContent = newContent[:len(newContent)-1]
			continue
		}
		if strings.HasPrefix(content[i], "Детализация") {
			i += 9 - 1
			continue
		}
		newContent = append(newContent, content[i])
	}
	//for _, line := range newContent[:70*2*8] {
	//fmt.Println(line)
	//}

	var tabel []Row
	for i := 0; i < len(newContent); i++ {
		var temp Row
		if newContent[i+2] == "Интернет" || newContent[i+2][0] == '+' {
			if newContent[i+3] == "Исходящая связь (справка)" {
				temp = Row{
					Data:     newContent[i],
					Time:     newContent[i+1],
					Number:   newContent[i+2],
					Type:     newContent[i+3],
					Position: newContent[i+4],
					Volume:   newContent[i+5],
					Price:    newContent[i+6],
				}
				i += 6
			} else {
				if newContent[i+3] == "Входящая  связь (справка)" {
					temp = Row{
						Data:   newContent[i],
						Time:   newContent[i+1],
						Number: newContent[i+2],
						Type:   newContent[i+3],
						Target: newContent[i+4],
						Volume: newContent[i+5],
						Price:  newContent[i+6],
					}
					i += 6
				} else {
					Position := newContent[i+4]
					Target := newContent[i+5]
					if Position == " " {
						Position = ""
					}
					if Target == " " {
						Target = ""
					}
					temp = Row{
						Data:     newContent[i],
						Time:     newContent[i+1],
						Number:   newContent[i+2],
						Type:     newContent[i+3],
						Position: Position,
						Target:   Target,
						Volume:   newContent[i+6],
						Price:    newContent[i+7],
					}
					i += 7
				}
			}
		} else {
			temp = Row{
				Data:   newContent[i],
				Time:   newContent[i+1],
				Number: newContent[i+2],
				Type:   newContent[i+3],
				Target: newContent[i+4],
				Volume: newContent[i+5],
				Price:  newContent[i+6],
			}
			i += 6
		}
		tabel = append(tabel, temp)
	}
	return tabel, nil
}

func readPdf2(path string, password string) ([]string, error) {
	pasFun := func() string { return password }
	f, r, err := openEncrypted(path, pasFun)
	// remember close file
	defer f.Close()
	if err != nil {
		return make([]string, 0), err
	}
	totalPage := r.NumPage()

	var array []string
	for pageIndex := 1; pageIndex <= totalPage; pageIndex++ {
		p := r.Page(pageIndex)
		if p.V.IsNull() {
			continue
		}
		var lastTextStyle pdf.Text
		texts := p.Content().Text
		for _, text := range texts {
			if isSameSentence(text, lastTextStyle) {
				lastTextStyle.S = lastTextStyle.S + text.S
			} else {
				array = append(array, lastTextStyle.S)
				//fmt.Printf("x: %f, y: %f, %s \n",
				//	lastTextStyle.X, lastTextStyle.Y, lastTextStyle.S)
				lastTextStyle = text
			}
		}
	}
	return array, nil
}

func isSameSentence(text pdf.Text, style pdf.Text) bool {
	if text.X == style.X { //} && text.Y == style.Y  {
		return true
	}
	return false
}

// Open opens a file for reading.
func openEncrypted(file string, passwd func() string) (*os.File, *pdf.Reader, error) {
	f, err := os.Open(file)
	if err != nil {
		f.Close()
		return nil, nil, err
	}
	fi, err := f.Stat()
	if err != nil {
		f.Close()
		return nil, nil, err
	}
	reader, err := pdf.NewReaderEncrypted(f, fi.Size(), passwd)
	return f, reader, err
}
